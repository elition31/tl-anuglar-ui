import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormsModule } from "@angular/forms";
import { OverlayModule } from "@angular/cdk/overlay";
import { BasicSpinnerModule } from "libs/tl-angular-ui/src/lib/modules/basic-spinner/basic-spinner.module";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, CommonModule, AppRoutingModule, FormsModule, OverlayModule, BasicSpinnerModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
