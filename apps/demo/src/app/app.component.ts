import { Component } from "@angular/core";
import {
  BASIC_SPINNER_CATEGORY,
  BASIC_SPINNER_SIZE,
  BarSpinner,
  BasicSpinnerOptions,
  CircleSpinner,
  DotSpinner,
  SquareSpinner,
} from "libs/tl-angular-ui/src/lib/modules/basic-spinner/basic-spinner.api";
import { Colors } from "libs/tl-angular-ui/src/lib/modules/shared/types/colors.type";

@Component({
  selector: "demo-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  public colors: Colors[] = ["primary", "secondary", "tertiary", "white", "black", "grey", "success", "error", "warning", "info"];
  public selectedColor: Colors = "primary";
  public sizes = Object.values(BASIC_SPINNER_SIZE);
  public selectedSize: BASIC_SPINNER_SIZE = BASIC_SPINNER_SIZE.XXL;
  public categories = Object.values(BASIC_SPINNER_CATEGORY);
  public selectedCategory: BASIC_SPINNER_CATEGORY = BASIC_SPINNER_CATEGORY.BAR;

  private circleSpinnerOptions: CircleSpinner = {
    isPathDouble: true,
    percentage: 50,
    isBackgroundPathDisplayed: true,
  };

  private dotSpinnerOptions: DotSpinner = {
    isBackgroundPathDisplayed: true,
    animationType: 3,
    quantity: 3,
  };

  private barSpinnerOptions: BarSpinner = {
    animationType: 4,
    quantity: 5,
  };

  private squareSpinnerOptions: SquareSpinner = {
    animationType: 2,
    hasRoundedBorder: true,
  };

  public basicSpinnerOptions: BasicSpinnerOptions<DotSpinner | CircleSpinner | SquareSpinner | BarSpinner> = {
    color: this.selectedColor,
    size: this.selectedSize,
    speed: 1.2,
    data: this.barSpinnerOptions,
    category: this.selectedCategory,
  };

  // TODO :
  // - Basic Spinner (Plusieurs style, mettre à jour le composant LOADER pour permettre de choisir le style de spinner)
  // - Implémenter le système de thème
  // - Progress Spinner
  // - Progress Bar (type: ROUND | LINE)
  // - Button

  constructor() {}

  public changeColor(color: Colors): void {
    this.basicSpinnerOptions.color = color;
  }

  public changeCategory(category: BASIC_SPINNER_CATEGORY): void {
    this.basicSpinnerOptions.category = category;
    if (category === BASIC_SPINNER_CATEGORY.CIRCLE) {
      this.basicSpinnerOptions.data = this.circleSpinnerOptions;
    } else if (category === BASIC_SPINNER_CATEGORY.DOT) {
      this.basicSpinnerOptions.data = this.dotSpinnerOptions;
    } else if (category === BASIC_SPINNER_CATEGORY.SQUARE) {
      this.basicSpinnerOptions.data = this.squareSpinnerOptions;
    } else if (category === BASIC_SPINNER_CATEGORY.BAR) {
      this.basicSpinnerOptions.data = this.barSpinnerOptions;
    }
  }

  public changeSize(size: BASIC_SPINNER_SIZE): void {
    this.basicSpinnerOptions.size = size;
  }
}
