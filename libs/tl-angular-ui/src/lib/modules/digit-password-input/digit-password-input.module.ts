import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { DigitPasswordInputComponent } from "./components/digit-password-input/digit-password-input.component";

@NgModule({
  declarations: [DigitPasswordInputComponent],
  imports: [SharedModule],
  exports: [DigitPasswordInputComponent],
})
export class DigitPasswordInputModule {}
