// Angular
import { Injectable } from "@angular/core";

// Libraries
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DigitPasswordInputService {
  private readonly resetAsked$ = new Subject<void>();

  /**
   * Reset the Random Number Input values
   */
  public reset(): void {
    this.resetAsked$.next();
  }

  /**
   * Emit an event when a reset is asking
   */
  public resetAsked(): Observable<void> {
    return this.resetAsked$;
  }
}
