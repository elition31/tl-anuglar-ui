import { TestBed } from "@angular/core/testing";

import { DigitPasswordInputService } from "./digit-password-input.service";

describe("RandomNumberInputService", () => {
  let service: DigitPasswordInputService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DigitPasswordInputService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
