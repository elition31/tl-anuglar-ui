// Angular
import { Component, EventEmitter, Output, OnInit, Input } from "@angular/core";

// Interfaces
import { DigitPasswordInputOptions } from "../../interfaces/digit-password-input-options.interface";
import { DigitPasswordInput } from "../../interfaces/digit-password-input.interface";

// Services
import { DigitPasswordInputService } from "../../services/digit-password-input.service";

// Libraries
import { Subject, takeUntil, tap } from "rxjs";

@Component({
  selector: "tl-digit-password-input",
  templateUrl: "./digit-password-input.component.html",
  styleUrls: ["./digit-password-input.component.scss"],
})
export class DigitPasswordInputComponent implements OnInit {
  @Output() clicked: EventEmitter<DigitPasswordInput> = new EventEmitter<DigitPasswordInput>();
  @Input({ required: true }) digitPasswordInputOptions: DigitPasswordInputOptions;

  private readonly destroy$ = new Subject<void>();
  private digitPasswordInput: DigitPasswordInput = { lastNumberClicked: null, numbersClicked: [] };

  public digits: number[];
  public selectedDigits: { value: number | null }[];

  constructor(private randomNumberInputService: DigitPasswordInputService) {}

  ngOnInit(): void {
    this.digits = this.initializeDigits();
    this.selectedDigits = this.initializeSelectedDigits();

    this.randomNumberInputService
      .resetAsked()
      .pipe(
        tap(() => {
          this.digitPasswordInput = { lastNumberClicked: null, numbersClicked: [] };
          this.digits = this.initializeDigits();
          this.clicked.emit(this.digitPasswordInput);
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public click(value: number): void {
    if (this.digitPasswordInput.numbersClicked.length < this.digitPasswordInputOptions.passwordLength) {
      this.digitPasswordInput.lastNumberClicked = value;
      this.digitPasswordInput.numbersClicked.push(value);
      this.selectedDigits[this.digitPasswordInput.numbersClicked.length - 1] = { value };
      this.clicked.emit(this.digitPasswordInput);
    }
  }

  public delete(): void {
    if (this.digitPasswordInput.numbersClicked.length > 0) {
      this.digitPasswordInput.numbersClicked.pop();

      const index = this.digitPasswordInput.numbersClicked.length;

      this.digitPasswordInput.lastNumberClicked = this.digitPasswordInput.numbersClicked[index - 1] ?? null;
      this.selectedDigits[index] = { value: null };
      this.clicked.emit(this.digitPasswordInput);
    }
  }

  private initializeDigits(): number[] {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].sort(() => Math.random() - 0.5);
  }

  private initializeSelectedDigits(): { value: number | null }[] {
    const result: { value: number | null }[] = [];

    for (let i = 0; i < this.digitPasswordInputOptions.passwordLength; i++) {
      result.push({ value: null });
    }

    return result;
  }
}
