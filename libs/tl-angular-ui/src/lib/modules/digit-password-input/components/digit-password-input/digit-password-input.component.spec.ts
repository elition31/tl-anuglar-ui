import { ComponentFixture, TestBed } from "@angular/core/testing";

import { DigitPasswordInputComponent } from "./digit-password-input.component";

describe("RandomNumberInputComponent", () => {
  let component: DigitPasswordInputComponent;
  let fixture: ComponentFixture<DigitPasswordInputComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DigitPasswordInputComponent],
    });
    fixture = TestBed.createComponent(DigitPasswordInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
