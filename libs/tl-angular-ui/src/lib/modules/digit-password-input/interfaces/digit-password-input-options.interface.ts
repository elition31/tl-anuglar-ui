import { Colors } from "../../shared/types/colors.type";
import { DIGIT_PASSWORD_INPUT_POSITION } from "../enums/digit-password-input-position.enum";
import { DIGIT_PASSWORD_INPUT_STYLE } from "../enums/digit-password-input-style.enum";

export interface DigitPasswordInputOptions {
  style: DIGIT_PASSWORD_INPUT_STYLE;
  color: Colors;
  inputPosition: DIGIT_PASSWORD_INPUT_POSITION;
  inputResultHidden: boolean;
  passwordLength: number;
  whiteBackgroundEnabled: boolean;
}
