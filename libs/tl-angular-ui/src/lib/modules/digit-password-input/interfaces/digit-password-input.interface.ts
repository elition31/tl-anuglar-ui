export interface DigitPasswordInput {
  lastNumberClicked: number | null;
  numbersClicked: number[];
}
