// Modules
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "../shared/shared.module";

// Directives
import { CircleDirective } from "./directives/circle.directive";
import { SquareDirective } from "./directives/square.directive";
import { DotDirective } from "./directives/dot.directive";
import { BarDirective } from "./directives/bar.directive";

// Components
import { BasicSpinnerComponent } from "./components/basic-spinner/basic-spinner.component";
import { CircleComponent } from "./components/circle/circle.component";
import { SquareComponent } from "./components/square/square.component";
import { DotComponent } from "./components/dot/dot.component";
import { BarComponent } from "./components/bar/bar.component";

const CIRCLE = [CircleComponent, CircleDirective];
const SQUARE = [SquareComponent, SquareDirective];
const DOT = [DotComponent, DotDirective];
const BAR = [BarComponent, BarDirective];

@NgModule({
  declarations: [BasicSpinnerComponent, ...CIRCLE, ...DOT, ...SQUARE, ...BAR],
  exports: [BasicSpinnerComponent],
  imports: [SharedModule],
})
export class BasicSpinnerModule {}
