// Angular
import { AfterViewInit, Directive, ElementRef, Input, OnChanges, Renderer2 } from "@angular/core";

// Enums
import { BASIC_SPINNER_SIZE } from "../enums/basic-spinner-size.enum";

// Interfaces
import { BasicSpinnerOptions } from "../interfaces/basic-spinner-options.interface";
import { CircleSpinner } from "../interfaces/circle-spinner.interface";

const MULTIPLICATOR: number = 6.29;

@Directive({
  selector: "[tlCircle]",
})
export class CircleDirective implements AfterViewInit, OnChanges {
  @Input({ required: true }) options: BasicSpinnerOptions<CircleSpinner>;

  private sizeAsNumber: number = 70;
  private strokeWidth: number = 10;
  private strokeValue: number = 440;

  constructor(
    private readonly renderer: Renderer2,
    private readonly element: ElementRef
  ) {}

  ngOnChanges(): void {
    this.sizeAsNumber = this.getSize();
    this.strokeValue = this.sizeAsNumber * MULTIPLICATOR;
    this.strokeWidth = this.getStrokeWidth();
  }

  ngAfterViewInit(): void {
    this.handleSpeed();
    this.handleContainerSize();
    this.handleCircleSize();
  }

  private handleSpeed(): void {
    this.renderer.setStyle(this.element.nativeElement, "animation", `rotate-center ${this.options.speed}s linear infinite`);
  }

  private handleContainerSize(): void {
    this.element.nativeElement.style.width = this.sizeAsNumber * 2 + this.strokeWidth + "px";
    this.element.nativeElement.style.height = this.sizeAsNumber * 2 + this.strokeWidth + "px";
  }

  private handleCircleSize(): void {
    const circles: HTMLCollection = this.element.nativeElement.getElementsByTagName("circle");
    const circlePath: HTMLCollection = this.element.nativeElement.getElementsByClassName("path");

    this.renderer.setStyle(circlePath.item(0), "cx", this.sizeAsNumber);
    this.renderer.setStyle(circlePath.item(0), "cy", this.sizeAsNumber);
    this.renderer.setStyle(circlePath.item(0), "r", this.sizeAsNumber);
    this.renderer.setStyle(circlePath.item(0), "strokeWidth", this.strokeWidth);
    this.renderer.setStyle(circlePath.item(0), "transform", `translate(${this.strokeWidth / 2}px, ${this.strokeWidth / 2}px)`);
    this.renderer.setStyle(circlePath.item(0), "strokeDashoffset", this.strokeValue);
    this.renderer.setStyle(circlePath.item(0), "strokeDasharray", this.options.data.isPathDouble ? this.strokeValue / 4 : this.strokeValue);

    if (circles.length === 1) {
      this.renderer.setStyle(circlePath.item(0), "strokeDashoffset", this.strokeValue - (this.strokeValue * this.options.data.percentage) / 100);
    } else {
      const circleBackPath: HTMLCollection = this.element.nativeElement.getElementsByClassName("back-path");

      this.renderer.setStyle(circleBackPath.item(0), "cx", this.sizeAsNumber);
      this.renderer.setStyle(circleBackPath.item(0), "cy", this.sizeAsNumber);
      this.renderer.setStyle(circleBackPath.item(0), "r", this.sizeAsNumber);
      this.renderer.setStyle(circleBackPath.item(0), "transform", `translate(${this.strokeWidth / 2}px, ${this.strokeWidth / 2}px)`);
      this.renderer.setStyle(circleBackPath.item(0), "strokeWidth", this.strokeWidth);
      this.renderer.setStyle(circlePath.item(0), "strokeDashoffset", this.strokeValue - (this.strokeValue * this.options.data.percentage) / 100);
    }
  }

  private getSize(): number {
    switch (this.options.size) {
      case BASIC_SPINNER_SIZE.XXXS:
        return 8;

      case BASIC_SPINNER_SIZE.XXS:
        return 16;

      case BASIC_SPINNER_SIZE.XS:
        return 30;

      case BASIC_SPINNER_SIZE.S:
        return 50;

      case BASIC_SPINNER_SIZE.M:
        return 70;

      case BASIC_SPINNER_SIZE.L:
        return 90;

      case BASIC_SPINNER_SIZE.XL:
        return 110;

      case BASIC_SPINNER_SIZE.XXL:
        return 140;

      default:
        return 50;
    }
  }

  private getStrokeWidth(): number {
    switch (this.options.size) {
      case BASIC_SPINNER_SIZE.XXXS:
        return 2;

      case BASIC_SPINNER_SIZE.XXS:
        return 4;

      case BASIC_SPINNER_SIZE.XS:
        return 6;

      case BASIC_SPINNER_SIZE.S:
        return 6;

      case BASIC_SPINNER_SIZE.M:
        return 8;

      case BASIC_SPINNER_SIZE.L:
        return 10;

      case BASIC_SPINNER_SIZE.XL:
        return 10;

      case BASIC_SPINNER_SIZE.XXL:
        return 12;

      default:
        return 8;
    }
  }
}
