// Angular
import { AfterViewInit, Directive, ElementRef, Input, OnChanges, Renderer2 } from "@angular/core";

// Enums
import { BASIC_SPINNER_SIZE } from "../enums/basic-spinner-size.enum";

// Interfaces
import { BasicSpinnerOptions } from "../interfaces/basic-spinner-options.interface";
import { DotSpinner } from "../interfaces/dot-spinner.interface";

@Directive({
  selector: "[tlDot]",
})
export class DotDirective implements AfterViewInit, OnChanges {
  @Input({ required: true }) options: BasicSpinnerOptions<DotSpinner>;

  private sizeAsNumber: number = 24;
  private speedRecalculated: number = 1000;
  private readonly gap: number = 4;

  constructor(
    private readonly renderer: Renderer2,
    private readonly element: ElementRef
  ) {}

  ngOnChanges(): void {
    this.sizeAsNumber = this.getSize();
    this.speedRecalculated = this.options.speed * 1000;
  }

  ngAfterViewInit(): void {
    this.handleContainerSize();
    this.handleDotSize();
    this.handleDotBackSize();
  }

  private handleContainerSize(): void {
    this.element.nativeElement.style.width = this.sizeAsNumber * this.options.data.quantity + this.gap * this.options.data.quantity + "px";
    this.element.nativeElement.style.height = `${this.sizeAsNumber}px`;
  }

  private handleDotSize(): void {
    const dotElements: HTMLCollection = this.element.nativeElement.getElementsByClassName("dot");

    for (let i = 0; i < dotElements.length; i++) {
      const element: Element = dotElements[i];
      const animation = this.getAnimation();

      element.animate(
        [
          { transform: `scale(${animation[0]})` },
          { transform: `scale(${animation[1]})` },
          { transform: `scale(${animation[2]})` },
          { transform: `scale(${animation[3]})` },
          { transform: `scale(${animation[4]})` },
          { transform: `scale(${animation[5]})` },
        ],
        {
          duration: this.speedRecalculated,
          iterations: Infinity,
          fill: "both",
          easing: "ease-in-out",
          delay: -(this.speedRecalculated - (this.speedRecalculated / this.options.data.quantity) * (i - 1)) / this.options.data.quantity,
        }
      );

      this.renderer.setStyle(element, "width", `${this.sizeAsNumber}px`);
      this.renderer.setStyle(element, "height", `${this.sizeAsNumber}px`);
    }
  }

  private handleDotBackSize(): void {
    const dotElements: HTMLCollection = this.element.nativeElement.getElementsByClassName("dot-back");

    if (dotElements.length > 0) {
      for (let i = 0; i < dotElements.length; i++) {
        const element: Element = dotElements[i];
        this.renderer.setStyle(element, "width", `${this.sizeAsNumber}px`);
        this.renderer.setStyle(element, "height", `${this.sizeAsNumber}px`);
      }
    }
  }

  private getSize(): number {
    switch (this.options.size) {
      case BASIC_SPINNER_SIZE.XXXS:
        return 8;

      case BASIC_SPINNER_SIZE.XXS:
        return 12;

      case BASIC_SPINNER_SIZE.XS:
        return 16;

      case BASIC_SPINNER_SIZE.S:
        return 20;

      case BASIC_SPINNER_SIZE.M:
        return 24;

      case BASIC_SPINNER_SIZE.L:
        return 28;

      case BASIC_SPINNER_SIZE.XL:
        return 32;

      case BASIC_SPINNER_SIZE.XXL:
        return 38;

      default:
        return 16;
    }
  }

  private getAnimation(): number[] {
    switch (this.options.data.animationType) {
      case 1:
        return [1, 0, 1, 0, 1, 1];

      case 2:
        return [1, 0, 1, 0, 0, 1];

      case 3:
        return [0, 0, 1, 0, 0, 0];

      case 4:
        return [0, 1, 0, 0, 1, 0];

      default:
        return [1, 0, 1, 0, 1, 1];
    }
  }
}
