// Angular
import { AfterViewInit, Directive, ElementRef, Input, OnChanges, Renderer2 } from "@angular/core";

// Enums
import { BASIC_SPINNER_SIZE } from "../enums/basic-spinner-size.enum";

// Interfaces
import { BasicSpinnerOptions } from "../interfaces/basic-spinner-options.interface";
import { BarSpinner } from "../interfaces/bar-spinner.interface";
import { Animation } from "../../shared/interfaces/animation.interface";

// Constants
import { BAR_ANIMATION_1_KEYFRAME, BAR_ANIMATION_2_KEYFRAME, BAR_ANIMATION_3_KEYFRAME, BAR_ANIMATION_4_KEYFRAME } from "../constants/bar-animations.const";

@Directive({
  selector: "[tlBar]",
})
export class BarDirective implements AfterViewInit, OnChanges {
  @Input({ required: true }) options: BasicSpinnerOptions<BarSpinner>;

  private sizeAsNumber: { width: number; height: number; gap: number } = { width: 4, height: 30, gap: 2 };
  private speedRecalculated: number = 1000;

  constructor(
    private readonly renderer: Renderer2,
    private readonly element: ElementRef
  ) {}

  ngOnChanges(): void {
    this.sizeAsNumber = this.getSize();
    this.speedRecalculated = this.options.speed * 1000;
  }

  ngAfterViewInit(): void {
    this.handleContainerSize();
    this.handleBarSize();
  }

  private handleContainerSize(): void {
    this.element.nativeElement.style.width = this.sizeAsNumber.width * this.options.data.quantity + this.sizeAsNumber.gap * this.options.data.quantity + "px";
    this.element.nativeElement.style.height = `${this.sizeAsNumber.height * this.getContainerHeightMultiplicatorBasedOnAnimation()}px`;
  }

  private handleBarSize() {
    const barElements: HTMLCollection = this.element.nativeElement.getElementsByClassName("bar");

    for (let i = 0; i < barElements.length; i++) {
      const element: Element = barElements[i];
      const animation: Animation = this.getAnimation();
      const delay: number = -(this.speedRecalculated - 100 * i);

      element.animate(animation.keyFrames, {
        ...animation.options,
        delay,
      });

      this.renderer.setStyle(element, "width", `${this.sizeAsNumber.width}px`);
      this.renderer.setStyle(element, "height", `${this.sizeAsNumber.height}px`);
    }
  }

  private getSize(): { width: number; height: number; gap: number } {
    switch (this.options.size) {
      case BASIC_SPINNER_SIZE.XXXS:
        return { width: 3, height: 12, gap: 1.5 };

      case BASIC_SPINNER_SIZE.XXS:
        return { width: 3, height: 16, gap: 1.5 };

      case BASIC_SPINNER_SIZE.XS:
        return { width: 3, height: 24, gap: 2 };

      case BASIC_SPINNER_SIZE.S:
        return { width: 3, height: 30, gap: 2 };

      case BASIC_SPINNER_SIZE.M:
        return { width: 4, height: 36, gap: 2 };

      case BASIC_SPINNER_SIZE.L:
        return { width: 6, height: 50, gap: 3 };

      case BASIC_SPINNER_SIZE.XL:
        return { width: 8, height: 54, gap: 3 };

      case BASIC_SPINNER_SIZE.XXL:
        return { width: 10, height: 90, gap: 4 };

      default:
        return { width: 4, height: 30, gap: 2 };
    }
  }

  private getContainerHeightMultiplicatorBasedOnAnimation(): number {
    switch (this.options.data.animationType) {
      case 1:
        return 2.5;
      case 2:
        return 2;
      case 3:
        return 1;
      case 4:
        return 2;
    }
  }

  private getAnimation(): Animation {
    switch (this.options.data.animationType) {
      case 1:
        return {
          keyFrames: BAR_ANIMATION_1_KEYFRAME,
          options: {
            iterations: Infinity,
            easing: "ease-in-out",
            duration: this.speedRecalculated,
          },
        };
      case 2:
        return {
          keyFrames: BAR_ANIMATION_2_KEYFRAME,
          options: {
            iterations: Infinity,
            easing: "ease-in-out",
            duration: this.speedRecalculated,
          },
        };
      case 3:
        return {
          keyFrames: BAR_ANIMATION_3_KEYFRAME,
          options: {
            iterations: Infinity,
            easing: "ease-in-out",
            duration: this.speedRecalculated,
          },
        };
      case 4:
        return {
          keyFrames: BAR_ANIMATION_4_KEYFRAME,
          options: {
            iterations: Infinity,
            easing: "ease-in-out",
            duration: this.speedRecalculated,
          },
        };
    }
  }
}
