// Angular
import { AfterViewInit, Directive, ElementRef, Input, OnChanges, Renderer2 } from "@angular/core";

// Enums
import { BASIC_SPINNER_SIZE } from "../enums/basic-spinner-size.enum";

// Interfaces
import { BasicSpinnerOptions } from "../interfaces/basic-spinner-options.interface";
import { SquareSpinner } from "../interfaces/square-spinner.interface";
import { Animation } from "../../shared/interfaces/animation.interface";

// Constants
import { SQUARE_ANIMATION_1_KEYFRAME, SQUARE_ANIMATION_2_KEYFRAME } from "../constants/square-animations.const";

@Directive({
  selector: "[tlSquare]",
})
export class SquareDirective implements AfterViewInit, OnChanges {
  @Input({ required: true }) options: BasicSpinnerOptions<SquareSpinner>;

  private sizeAsNumber: number = 24;
  private speedRecalculated: number = 1000;

  constructor(
    private readonly renderer: Renderer2,
    private readonly element: ElementRef
  ) {}

  ngOnChanges(): void {
    this.sizeAsNumber = this.getSize();
    this.speedRecalculated = this.options.speed * 1000;
  }

  ngAfterViewInit(): void {
    this.handleContainerSize();
    this.handleSquareSize();
  }

  private handleContainerSize(): void {
    this.element.nativeElement.style.width = `${this.sizeAsNumber + 20}px`;
    this.element.nativeElement.style.height = `${this.sizeAsNumber + 20}px`;
  }

  private handleSquareSize(): void {
    switch (this.options.data.animationType) {
      case 1:
        this.handleAnimationOne();
        break;
      case 2:
        this.handleAnimationTwo();
        break;

      default:
        this.handleAnimationOne();
        break;
    }
  }

  private handleAnimationOne(): void {
    const animationElement: HTMLCollection = this.element.nativeElement.getElementsByClassName("animation-1");
    this.renderer.setStyle(animationElement.item(0), "width", `${this.sizeAsNumber}px`);
    this.renderer.setStyle(animationElement.item(0), "height", `${this.sizeAsNumber}px`);

    const animation: Animation = this.getAnimation();
    animationElement.item(0).animate(animation.keyFrames, animation.options);
  }

  private handleAnimationTwo(): void {
    const animationElement: HTMLCollection = this.element.nativeElement.getElementsByClassName("animation-2");
    this.renderer.setStyle(animationElement.item(0), "width", `${this.sizeAsNumber}px`);
    this.renderer.setStyle(animationElement.item(0), "height", `${this.sizeAsNumber}px`);

    const delays: number[] = [0.2, 0.3, 0.4, 0.1, 0.2, 0.3, 0, 0.1, 0.2];
    const cubeElements: HTMLCollection = animationElement.item(0).getElementsByClassName("cube");
    const animation: Animation = this.getAnimation();

    for (let i = 0; i < cubeElements.length; i++) {
      const element: Element = cubeElements[i];
      element.animate(animation.keyFrames, { ...animation.options, delay: delays[i] * 1000 });
    }
  }

  private getSize(): number {
    switch (this.options.size) {
      case BASIC_SPINNER_SIZE.XXXS:
        return 16;

      case BASIC_SPINNER_SIZE.XXS:
        return 30;

      case BASIC_SPINNER_SIZE.XS:
        return 40;

      case BASIC_SPINNER_SIZE.S:
        return 50;

      case BASIC_SPINNER_SIZE.M:
        return 60;

      case BASIC_SPINNER_SIZE.L:
        return 80;

      case BASIC_SPINNER_SIZE.XL:
        return 90;

      case BASIC_SPINNER_SIZE.XXL:
        return 120;

      default:
        return 60;
    }
  }

  private getAnimation(): Animation {
    switch (this.options.data.animationType) {
      case 1:
        return {
          keyFrames: SQUARE_ANIMATION_1_KEYFRAME,
          options: {
            iterations: Infinity,
            duration: this.speedRecalculated,
            easing: "ease-in-out",
          },
        };

      case 2:
        return {
          keyFrames: SQUARE_ANIMATION_2_KEYFRAME,
          options: {
            iterations: Infinity,
            duration: this.speedRecalculated,
            easing: "ease-in-out",
          },
        };

      default:
        return {
          keyFrames: SQUARE_ANIMATION_1_KEYFRAME,
          options: {
            iterations: Infinity,
            duration: this.speedRecalculated,
            easing: "ease-in-out",
          },
        };
    }
  }
}
