export const BAR_ANIMATION_1_KEYFRAME: Keyframe[] = [
  {
    transform: "scaleY(1.0)",
  },
  {
    transform: "scaleY(2.5)",
  },
  {
    transform: "scaleY(1.0)",
  },
  {
    transform: "scaleY(1.0)",
  },
];

export const BAR_ANIMATION_2_KEYFRAME: Keyframe[] = [
  {
    transform: "scaleY(1.0)",
  },
  {
    transform: "scaleY(0.6)",
  },
  {
    transform: "scaleY(2.0)",
  },
  {
    transform: "scaleY(0.6)",
  },
  {
    transform: "scaleY(0.6)",
  },
];

export const BAR_ANIMATION_3_KEYFRAME: Keyframe[] = [
  {
    transform: "scaleY(1.0)",
  },
  {
    transform: "scaleY(0.8)",
  },
  {
    transform: "scaleY(0.6)",
  },
  {
    transform: "scaleY(0.4)",
  },
  {
    transform: "scaleY(0.2)",
  },
  {
    transform: "scaleY(0)",
  },
  {
    transform: "scaleY(0.2)",
  },
  {
    transform: "scaleY(0.4)",
  },
  {
    transform: "scaleY(0.6)",
  },
  {
    transform: "scaleY(0.8)",
  },
  {
    transform: "scaleY(1.0)",
  },
];

export const BAR_ANIMATION_4_KEYFRAME: Keyframe[] = [
  {
    transform: "scaleY(1.0)",
  },
  {
    transform: "scaleY(1.2)",
  },
  {
    transform: "scaleY(1.4)",
  },
  {
    transform: "scaleY(1.6)",
  },
  {
    transform: "scaleY(1.8)",
  },
  {
    transform: "scaleY(2.0)",
  },
  {
    transform: "scaleY(1.8)",
  },
  {
    transform: "scaleY(1.6)",
  },
  {
    transform: "scaleY(1.4)",
  },
  {
    transform: "scaleY(1.2)",
  },
  {
    transform: "scaleY(1.0)",
  },
];
