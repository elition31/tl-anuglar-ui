export const SQUARE_ANIMATION_1_KEYFRAME: Keyframe[] = [
  {
    transform: "perspective(120px) rotateX(0deg) rotateY(0deg)",
  },
  {
    transform: "perspective(120px) rotateX(-180.1deg) rotateY(0deg)",
  },
  {
    transform: "perspective(120px) rotateX(-180deg) rotateY(-179.9deg)",
  },
];

export const SQUARE_ANIMATION_2_KEYFRAME: Keyframe[] = [
  {
    transform: "scale3D(1, 1, 1)",
  },
  {
    transform: "scale3D(0, 0, 1)",
  },
  {
    transform: "scale3D(1, 1, 1)",
  },
  {
    transform: "scale3D(1, 1, 1)",
  },
];
