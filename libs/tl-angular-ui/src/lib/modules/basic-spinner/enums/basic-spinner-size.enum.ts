export enum BASIC_SPINNER_SIZE {
  XXXS = "XXXS",
  XXS = "XXS",
  XS = "XS",
  S = "S",
  M = "M",
  L = "L",
  XL = "XL",
  XXL = "XXL",
}
