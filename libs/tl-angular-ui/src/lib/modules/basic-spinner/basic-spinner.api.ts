// Module
export { BasicSpinnerModule } from "./basic-spinner.module";

// Components
export { BasicSpinnerComponent } from "./components/basic-spinner/basic-spinner.component";

// Interfaces
export { BasicSpinnerOptions } from "./interfaces/basic-spinner-options.interface";
export { CircleSpinner } from "./interfaces/circle-spinner.interface";
export { SquareSpinner } from "./interfaces/square-spinner.interface";
export { DotSpinner } from "./interfaces/dot-spinner.interface";
export { BarSpinner } from "./interfaces/bar-spinner.interface";

// Enums
export { BASIC_SPINNER_SIZE } from "./enums/basic-spinner-size.enum";
export { BASIC_SPINNER_CATEGORY } from "./enums/basic-spinner-category.enum";
