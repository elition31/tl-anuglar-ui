export interface DotSpinner {
  quantity: 3 | 4 | 5 | 6;
  animationType: 1 | 2 | 3 | 4;
  isBackgroundPathDisplayed: boolean;
}
