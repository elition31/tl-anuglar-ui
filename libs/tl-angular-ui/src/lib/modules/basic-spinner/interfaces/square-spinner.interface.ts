export interface SquareSpinner {
  animationType: 1 | 2;
  hasRoundedBorder: boolean;
}
