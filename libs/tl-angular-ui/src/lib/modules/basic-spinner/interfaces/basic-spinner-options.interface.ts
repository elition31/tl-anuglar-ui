// Types
import { Colors } from "../../shared/types/colors.type";

// Enums
import { BASIC_SPINNER_CATEGORY } from "../enums/basic-spinner-category.enum";
import { BASIC_SPINNER_SIZE } from "../enums/basic-spinner-size.enum";

export interface BasicSpinnerOptions<T> {
  category: BASIC_SPINNER_CATEGORY;
  size: BASIC_SPINNER_SIZE;
  speed: number;
  color: Colors;
  data: T;
}
