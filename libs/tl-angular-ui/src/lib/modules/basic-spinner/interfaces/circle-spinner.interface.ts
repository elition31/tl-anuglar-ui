export interface CircleSpinner {
  percentage: 10 | 20 | 25 | 33 | 50 | 66 | 75 | 80;
  isPathDouble: boolean;
  isBackgroundPathDisplayed: boolean;
}
