// Angular
import { Component, Input } from "@angular/core";

// Interfaces
import { BasicSpinnerOptions } from "../../interfaces/basic-spinner-options.interface";
import { CircleSpinner } from "../../interfaces/circle-spinner.interface";

@Component({
  selector: "tl-circle",
  templateUrl: "./circle.component.html",
  styleUrls: ["./circle.component.scss"],
})
export class CircleComponent {
  @Input({ required: true }) options: BasicSpinnerOptions<CircleSpinner>;
}
