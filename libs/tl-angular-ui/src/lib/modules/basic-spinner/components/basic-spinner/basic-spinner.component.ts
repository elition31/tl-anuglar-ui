// Angular
import { Component, Input } from "@angular/core";

// Interfaces
import { BasicSpinnerOptions } from "../../interfaces/basic-spinner-options.interface";
import { CircleSpinner } from "../../interfaces/circle-spinner.interface";
import { SquareSpinner } from "../../interfaces/square-spinner.interface";
import { DotSpinner } from "../../interfaces/dot-spinner.interface";
import { BarSpinner } from "../../interfaces/bar-spinner.interface";

@Component({
  selector: "tl-basic-spinner",
  templateUrl: "./basic-spinner.component.html",
  styleUrls: ["./basic-spinner.component.scss"],
})
export class BasicSpinnerComponent {
  @Input({ required: true }) options: BasicSpinnerOptions<DotSpinner | CircleSpinner | SquareSpinner | BarSpinner>;

  public castAsCircleSpinnerData(): BasicSpinnerOptions<CircleSpinner> {
    return this.options as BasicSpinnerOptions<CircleSpinner>;
  }

  public castAsDotSpinnerData(): BasicSpinnerOptions<DotSpinner> {
    return this.options as BasicSpinnerOptions<DotSpinner>;
  }

  public castAsSquareSpinnerData(): BasicSpinnerOptions<SquareSpinner> {
    return this.options as BasicSpinnerOptions<SquareSpinner>;
  }

  public castAsBarSpinnerData(): BasicSpinnerOptions<BarSpinner> {
    return this.options as BasicSpinnerOptions<BarSpinner>;
  }
}
