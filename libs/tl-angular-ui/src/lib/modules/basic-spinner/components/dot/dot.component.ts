// Angular
import { Component, Input, OnChanges } from "@angular/core";

// Interfaces
import { BasicSpinnerOptions } from "../../interfaces/basic-spinner-options.interface";
import { DotSpinner } from "../../interfaces/dot-spinner.interface";

@Component({
  selector: "tl-dot",
  templateUrl: "./dot.component.html",
  styleUrls: ["./dot.component.scss"],
})
export class DotComponent implements OnChanges {
  @Input({ required: true }) options: BasicSpinnerOptions<DotSpinner>;

  public dots: number[] = [];

  ngOnChanges(): void {
    this.dots = [];

    for (let i = 1; i <= this.options.data.quantity; i++) {
      this.dots.push(i);
    }
  }
}
