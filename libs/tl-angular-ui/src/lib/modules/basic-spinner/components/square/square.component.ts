// Angular
import { Component, Input } from "@angular/core";

// Interfaces
import { BasicSpinnerOptions } from "../../interfaces/basic-spinner-options.interface";
import { SquareSpinner } from "../../interfaces/square-spinner.interface";

@Component({
  selector: "tl-square",
  templateUrl: "./square.component.html",
  styleUrls: ["./square.component.scss"],
})
export class SquareComponent {
  @Input({ required: true }) options: BasicSpinnerOptions<SquareSpinner>;
}
