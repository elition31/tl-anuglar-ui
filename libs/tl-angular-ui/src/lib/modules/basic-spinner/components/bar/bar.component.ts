// Angular
import { Component, Input, OnChanges } from "@angular/core";

// Interfaces
import { BarSpinner } from "../../interfaces/bar-spinner.interface";
import { BasicSpinnerOptions } from "../../interfaces/basic-spinner-options.interface";

@Component({
  selector: "tl-bar",
  templateUrl: "./bar.component.html",
  styleUrls: ["./bar.component.scss"],
})
export class BarComponent implements OnChanges {
  @Input({ required: true }) options: BasicSpinnerOptions<BarSpinner>;

  public bars: number[] = [];

  ngOnChanges(): void {
    this.bars = [];

    for (let i = 1; i <= this.options.data.quantity; i++) {
      this.bars.push(i);
    }
  }
}
