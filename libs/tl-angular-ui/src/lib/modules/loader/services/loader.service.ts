// Angular
import { Injectable } from "@angular/core";
import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";

// Interfaces
import { Loader } from "../interfaces/loader.interface";

// Libraries
import { BehaviorSubject, Observable, map, tap } from "rxjs";
import { v4 as uuidv4 } from "uuid";

// Components
import { LoaderComponent } from "../components/loader/loader.component";

@Injectable({
  providedIn: "root",
})
export class LoaderService {
  private loaders$: BehaviorSubject<Loader[]> = new BehaviorSubject<Loader[]>([]);
  private overlayReference: OverlayRef | undefined = undefined;

  constructor(private overlay: Overlay) {
    this.initializeLoadersSubscription();
  }

  /**
   * Get all the loaders.
   */
  public getLoaders(): Observable<Loader[]> {
    return this.loaders$;
  }

  /**
   * Get a loader by its ID
   */
  public getLoaderByItsId(id: string): Observable<Loader> {
    return this.loaders$.pipe(map((loaders) => loaders.find((loader) => loader.id === id)));
  }

  /**
   * Get the current loader
   */
  public getCurrentLoader(): Observable<Loader> {
    return this.loaders$.pipe(map((loaders) => loaders[0]));
  }

  /**
   * Add a loader and returns its updated instance
   */
  public addLoader(loader: Loader): Loader {
    loader = this.generateLoader(loader);
    this.loaders$.next([...this.loaders$.value, loader]);
    return loader;
  }

  /**
   * Update a loader by its ID.
   */
  public updateLoader(loader: Loader): void {
    this.loaders$.next(this.loaders$.value.map((loaderValue) => (loaderValue.id === loader.id ? loader : loaderValue)));
  }

  /**
   * Delete a loader by its ID.
   */
  public deleteLoaderById(id: string): void {
    const values: Loader[] = this.loaders$.value;
    values.splice(
      values.findIndex((v) => v.id === id),
      1
    );
    this.loaders$.next(values);
  }

  /**
   * Delete all the loaders.
   */
  public deleteAllLoaders(): void {
    this.loaders$.next([]);
  }

  /**
   * Check if the loader given still exists.
   */
  public isLoaderStillExists(loader: Loader): boolean {
    return this.loaders$.value.find((v) => v.id === loader.id) !== undefined;
  }

  /**
   * Generate unique loader ID.
   */
  private generateLoaderId(): string {
    return uuidv4();
  }

  /**
   * Generate a loader with generic values if not set
   */
  private generateLoader(loader: Loader): Loader {
    return {
      backgroundColor: loader.backgroundColor ?? "primary",
      spinnerColor: loader.spinnerColor ?? "white",
      messageColor: loader.messageColor ?? "primary",
      displayBackground: loader.displayBackground ?? true,
      id: loader.id ?? this.generateLoaderId(),
      state: loader.state ?? true,
      isMessageDisplayed: loader.isMessageDisplayed ?? false,
      message: loader.message ?? "",
      isBackgroundOpacityEnabled: loader.isBackgroundOpacityEnabled ?? false,
    };
  }

  private initializeLoadersSubscription(): void {
    this.loaders$
      .pipe(
        tap((loaders: Loader[]) => {
          if (loaders.length > 0 && !this.overlayReference) {
            this.overlayReference = this.overlay.create({
              height: "100%",
              width: "100%",
              positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
            });

            const loaderComponentPortal = new ComponentPortal(LoaderComponent);

            this.overlayReference.attach(loaderComponentPortal);
          } else if (loaders.length === 0 && this.overlayReference) {
            this.overlayReference.detach();
          }
        })
      )
      .subscribe();
  }
}
