// Angular
import { Directive, ElementRef, Input, OnChanges, Renderer2 } from "@angular/core";

// Types
import { Colors } from "../../shared/types/colors.type";

@Directive({
  selector: "[tlLoader]",
})
export class LoaderDirective implements OnChanges {
  @Input() messageColor: Colors;
  @Input() backgroundColor: Colors;
  @Input() isBackgroundOpacityEnabled: boolean;

  constructor(
    private renderer: Renderer2,
    private element: ElementRef
  ) {}

  ngOnChanges(): void {
    this.handleBackground();
    this.handleMessageColor();
  }

  private handleBackground(): void {
    let existingBackgroundClass: string;

    this.element.nativeElement.classList.forEach((cssClass: string) => {
      if (cssClass.includes("bg")) {
        existingBackgroundClass = cssClass;
      }
    });

    this.renderer.removeClass(this.element.nativeElement, existingBackgroundClass);
    this.renderer.addClass(this.element.nativeElement, `${this.backgroundColor}-${this.isBackgroundOpacityEnabled ? "bg-no-opacity" : "bg"}`);
  }

  private handleMessageColor() {
    this.renderer.addClass(this.element.nativeElement, `${this.messageColor}-text`);

    setTimeout(() => {
      const messageHtmlElement: HTMLCollection = this.element.nativeElement.getElementsByClassName("message");
      let existingBackgroundClass: string;

      messageHtmlElement.item(0).classList.forEach((cssClass: string) => {
        if (cssClass.includes("-text")) {
          existingBackgroundClass = cssClass;
        }
      });

      this.renderer.removeClass(this.element.nativeElement, existingBackgroundClass);
      this.renderer.addClass(this.element.nativeElement, `${this.messageColor}-text`);
    }, 100);
  }
}
