import { Colors } from "../../shared/types/colors.type";

export interface Loader {
  id?: string;
  state?: boolean;
  message?: string;
  isMessageDisplayed?: boolean;
  displayBackground?: boolean;
  isBackgroundOpacityEnabled?: boolean;
  backgroundColor?: Colors;
  spinnerColor?: Colors;
  messageColor?: Colors;
}
