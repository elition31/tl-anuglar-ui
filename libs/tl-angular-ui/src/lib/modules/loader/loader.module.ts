import { NgModule } from "@angular/core";
import { LoaderComponent } from "./components/loader/loader.component";
import { SharedModule } from "../shared/shared.module";
import { LoaderDirective } from "./directives/loader.directive";

@NgModule({
  declarations: [LoaderComponent, LoaderDirective],
  imports: [SharedModule],
  exports: [LoaderComponent],
})
export class LoaderModule {}
