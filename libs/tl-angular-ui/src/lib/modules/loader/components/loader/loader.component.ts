// Angular
import { Component } from "@angular/core";

// Libraries
import { Observable } from "rxjs";

// Models
import { Loader } from "../../interfaces/loader.interface";

// Services
import { LoaderService } from "../../services/loader.service";

@Component({
  selector: "tl-loader",
  templateUrl: "./loader.component.html",
  styleUrls: ["./loader.component.scss"],
})
export class LoaderComponent {
  public loader$: Observable<Loader>;

  constructor(private loaderService: LoaderService) {
    this.loader$ = this.loaderService.getCurrentLoader();
  }
}
