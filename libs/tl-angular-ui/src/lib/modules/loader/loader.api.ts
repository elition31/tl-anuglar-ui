export { LoaderModule } from "./loader.module";

// Services
export { LoaderService } from "./services/loader.service";

// Components
export { LoaderComponent } from "./components/loader/loader.component";

// Interfaces
export { Loader } from "./interfaces/loader.interface";
