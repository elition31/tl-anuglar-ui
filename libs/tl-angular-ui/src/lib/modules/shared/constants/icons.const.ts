// Librairies
import { faCircleInfo, IconDefinition, faXmark, faArrowLeftLong } from "@fortawesome/free-solid-svg-icons";
import { faCircleExclamation, faCircleXmark, faCircleCheck } from "@fortawesome/free-solid-svg-icons";

export const ICONS: { name: string; icon: IconDefinition }[] = [
  {
    name: "info-circle-filled",
    icon: faCircleInfo,
  },
  {
    name: "warning-circle-filled",
    icon: faCircleExclamation,
  },
  {
    name: "error-circle-filled",
    icon: faCircleXmark,
  },
  {
    name: "success-circle-filled",
    icon: faCircleCheck,
  },
  {
    name: "close",
    icon: faXmark,
  },
  {
    name: "arrow-left-long",
    icon: faArrowLeftLong,
  },
];
