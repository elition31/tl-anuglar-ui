// Angular
import { Component, Input } from "@angular/core";

// Types
import { Colors } from "../../types/colors.type";

@Component({
  selector: "tl-basic-spinner",
  templateUrl: "./basic-spinner.component.html",
  styleUrls: ["./basic-spinner.component.scss"],
})
export class BasicSpinnerComponent {
  @Input() color: Colors = "white";
  @Input() size: string = "100px";

  constructor() {}
}
