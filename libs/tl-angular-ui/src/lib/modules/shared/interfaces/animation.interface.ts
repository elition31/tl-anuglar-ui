export interface Animation {
  keyFrames: Keyframe[] | PropertyIndexedKeyframes;
  options: KeyframeAnimationOptions;
}
