import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ThemeService {
  private theme: BehaviorSubject<"dark" | "light"> = new BehaviorSubject<"dark" | "light">("light");

  public getTheme(): "dark" | "light" {
    return this.theme.value;
  }

  public getThemeAsObservable(): Observable<"dark" | "light"> {
    return this.theme;
  }

  public switchTheme(): void {
    const theme = this.theme.value === "light" ? "dark" : "light";
    this.theme.next(theme);
  }

  public setTheme(theme: "dark" | "light"): void {
    this.theme.next(theme);
  }
}
