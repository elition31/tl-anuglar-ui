// Angular
import { Pipe, PipeTransform } from "@angular/core";

// Constants
import { ICONS } from "../constants/icons.const";

// Libraries
import { IconDefinition } from "@fortawesome/fontawesome-common-types";

@Pipe({
  name: "getIcon",
})
export class getIconPipe implements PipeTransform {
  transform(level: string): IconDefinition {
    return ICONS.find((icon) => level == icon.name).icon;
  }
}
