// Angular
import { Pipe, PipeTransform } from "@angular/core";

// Types
import { Colors } from "../types/colors.type";

@Pipe({
  name: "colorToCssClass",
})
export class ColorToCssClassPipe implements PipeTransform {
  transform(color: Colors, suffix: string = ""): string {
    return `${color}-${suffix}`;
  }
}
