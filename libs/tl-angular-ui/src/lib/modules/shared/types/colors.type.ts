export type Colors = "primary" | "secondary" | "tertiary" | "white" | "black" | "grey" | "success" | "error" | "warning" | "info";
