// Angular
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { OverlayModule } from "@angular/cdk/overlay";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

// Components
import { BasicSpinnerComponent } from "./components/spinner/basic-spinner.component";

// Modules
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

// Pipes
import { ColorToCssClassPipe } from "./pipes/color-to-css-class.pipe";
import { getIconPipe } from "./pipes/get-icon.pipe";

const COMPONENTS = [BasicSpinnerComponent];
const MODULES = [CommonModule, OverlayModule, FontAwesomeModule, ReactiveFormsModule, FormsModule];
const PIPES = [ColorToCssClassPipe, getIconPipe];

@NgModule({
  declarations: [...COMPONENTS, ...PIPES],
  imports: [...MODULES],
  exports: [...MODULES, ...COMPONENTS, ...PIPES],
})
export class SharedModule {}
