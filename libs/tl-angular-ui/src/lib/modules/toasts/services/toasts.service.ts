// Angular
import { Injectable } from "@angular/core";

// Interfaces
import { Toast } from "../interfaces/toast.interface";
import { Positions } from "../interfaces/positions.interface";

// Libraries
import { BehaviorSubject } from "rxjs";
import { v4 as uuidv4 } from "uuid";

@Injectable({
  providedIn: "root",
})
export class ToastsService {
  private toasts$: BehaviorSubject<Toast[]> = new BehaviorSubject<Toast[]>([]);
  private positions: Positions = { x: "right", y: "bottom" };

  constructor() {}

  /**
   * Get all the toasts.
   */
  public getToasts(): BehaviorSubject<Toast[]> {
    return this.toasts$;
  }

  /**
   * Add a new toast.
   */
  public addToast(toast: Toast): void {
    toast.id = this.generateToastId();
    this.toasts$.next([...this.toasts$.value, toast]);
    this.removeToastWhenTimeIsOver(toast);
  }

  /**
   * Delete a toast by its ID.
   */
  public deleteToastById(id: string): void {
    const values: Toast[] = this.toasts$.value;
    values.splice(
      values.findIndex((v) => v.id === id),
      1
    );
    this.toasts$.next(values);
  }

  /**
   * Delete all the loaders.
   */
  public deleteAllToasts(): void {
    this.toasts$.next([]);
  }

  /**
   * Get the position of the toasts.
   */
  public getToastsPosition(): Positions {
    return this.positions;
  }

  /**
   * Select the position of the toasts.
   * Default is x: "right", y: "bottom".
   */
  public setToastsPosition(positions: Positions): void {
    this.positions = positions;
  }

  /**
   * Delete the toast when the given timer is over.
   * If the duration is 0, then the toast remains indefinitely until the user closes it manually.
   */
  private removeToastWhenTimeIsOver(toast: Toast): void {
    if (toast.duration > 0) {
      setTimeout(() => {
        if (this.toasts$.value.find((v) => v.id === toast.id) !== undefined) {
          const values: Toast[] = this.toasts$.value;
          values.splice(
            values.findIndex((v) => v.id === toast.id),
            1
          );
          this.toasts$.next(values);
        }
      }, toast.duration + 600);
    }
  }

  /**
   * Generate unique loader ID.
   */
  private generateToastId(): string {
    return uuidv4();
  }
}
