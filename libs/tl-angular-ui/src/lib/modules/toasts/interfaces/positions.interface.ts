import { PositionX } from "../types/position-x.type";
import { PositionY } from "../types/position-y.type";

export interface Positions {
  x: PositionX;
  y: PositionY;
}
