import { Level } from "../types/level.type";

export interface Toast {
  id?: string;
  duration: number;
  title: string;
  message: string;
  button?: string; // TODO : remove
  actions?: {
    primary: {
      buttonName: string;
      callback: Callback;
    };
    secondary?: {
      buttonName: string;
      callback: Callback;
    };
  };
  data?: any;
  level: Level;
}

interface Callback {
  (toast: Toast): void;
}
