// Angular
import { NgModule } from "@angular/core";

// Components
import { ToastComponent } from "./components/toast/toast.component";
import { ToastsComponent } from "./components/toasts/toasts.component";

// Modules
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [ToastComponent, ToastsComponent],
  imports: [SharedModule],
  exports: [ToastComponent, ToastsComponent],
})
export class ToastsModule {}
