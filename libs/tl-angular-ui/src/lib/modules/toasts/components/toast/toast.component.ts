// Angular
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

// Interfaces
import { Toast } from "../../interfaces/toast.interface";
import { Positions } from "../../interfaces/positions.interface";

@Component({
  selector: "tl-toast",
  templateUrl: "./toast.component.html",
  styleUrls: ["./toast.component.scss"],
})
export class ToastComponent implements OnInit {
  @Input() toast!: Toast;
  @Input({ required: true }) positions: Positions;
  @Output() closeEvent: EventEmitter<Toast> = new EventEmitter<Toast>();

  public width: number = 100;
  public isFinished: boolean = false;

  constructor() {}

  ngOnInit(): void {
    let duration = this.toast.duration;

    if (duration > 0) {
      const id = setInterval(() => {
        if (duration === 0) {
          this.isFinished = true;
          clearInterval(id);
        } else {
          duration -= 100;
          this.width = (duration * 100) / this.toast.duration;
        }
      }, 100);
    }
  }

  public close(): void {
    this.isFinished = true;
    setTimeout(() => {
      this.closeEvent.emit(this.toast);
    }, 600);
  }
}
