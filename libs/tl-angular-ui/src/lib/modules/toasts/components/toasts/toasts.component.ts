// Angular
import { Component } from "@angular/core";

// Services
import { ToastsService } from "../../services/toasts.service";

// Interfaces
import { Toast } from "../../interfaces/toast.interface";
import { Positions } from "../../interfaces/positions.interface";

@Component({
  selector: "tl-toasts",
  templateUrl: "./toasts.component.html",
  styleUrls: ["./toasts.component.scss"],
})
export class ToastsComponent {
  public toasts: Toast[] = [];
  public positions: Positions = { x: "right", y: "bottom" };

  constructor(private toastService: ToastsService) {
    this.positions = this.toastService.getToastsPosition();
    this.toastService.getToasts().subscribe((toasts) => {
      this.toasts = toasts;
    });
  }

  public close(toast: Toast): void {
    this.toastService.deleteToastById(toast.id!);
  }
}
