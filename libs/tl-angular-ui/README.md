# TlAngularUi

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.0.

## Code scaffolding

Run `ng generate component component-name --project tl-angular-ui` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project tl-angular-ui`.
> Note: Don't forget to add `--project tl-angular-ui` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build tl-angular-ui` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build tl-angular-ui`, go to the dist folder `cd dist/tl-angular-ui` and run `npm publish`.

## TODO
[x] Loader
[ ] Basic Spinner
[ ] Progress Spinner
[ ] Progress Bar
[ ] Reading Progress Bar
[x] Toasts
[ ] Alerts
[ ] Button
[ ] Input
[x] Digit Password Input
[ ] Select
[ ] Toggle
[ ] Tabs
[ ] Menu
[ ] Button Menu
[ ] Dialog
[ ] Bottom | Top | Left | Right Sheet
[ ] Card
[ ] Checkbox
[ ] Date Picker
[ ] Time Picker
[ ] Color Picker
[ ] Breadcrumbs
[ ] Accordion
[ ] Stepper
[ ] Table
[ ] Tree
[ ] Tag
[ ] Slider
[ ] Tooltip
[ ] Skeleton
[ ] Rating
[ ] Timeline
[ ] Theme Generator
[ ] Theme Service
[ ] Application de documentation
